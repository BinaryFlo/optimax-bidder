package auction;

/**
 * A tool that takes a sequence of values and estimates which value will come next.
 */
public interface AnalysisTool {

    /**
     * Adds a value to the sequence.
     *
     * @param value the value to be added
     */
    void addValue(int value);

    /**
     * Gives an estimation which value will come next.
     *
     * @return an estimation about the next value
     */
    Estimation getEstimation();

}
