package auction;

/**
 * A tool to analyse the bids of the opponent. The sub tools pattern recognition, linear regression and average value
 * calculation are used to estimate the next bid.
 */
public class OpponentAnalysis implements AnalysisTool {

    /**
     * Defines how many recent bids should be analysed by the linear regression tool and the average value tool.
     */
    private static final int MAX_NUMBER_OF_RECENT_BIDS_TO_ANALYSE = 8;

    /**
     * Defines the maximum difference two estimated values may have to be treated as nearby relative to the expected
     * average bid.
     */
    private static final double MAX_DIFFERENCE_FOR_BIDS_TO_BE_NEARBY_RELATIVE_TO_AVERAGE_BID = 0.2;

    private final AnalysisTool patternRecognitionTool;
    private final AnalysisTool linearRegressionTool;
    private final AnalysisTool averagePredictionTool;
    private final int acceptedDifferenceForBidsToBeNearby;

    /**
     * Initializes the tool with the expected average bid
     *
     * @param averageBid the expected average bid
     */
    public OpponentAnalysis(int averageBid) {
        patternRecognitionTool = new PatternRecognitionTool();
        linearRegressionTool = new LinearRegressionTool(MAX_NUMBER_OF_RECENT_BIDS_TO_ANALYSE);
        averagePredictionTool = new AveragePredictionTool(MAX_NUMBER_OF_RECENT_BIDS_TO_ANALYSE, averageBid);
        this.acceptedDifferenceForBidsToBeNearby =
                (int) Math.round(averageBid * MAX_DIFFERENCE_FOR_BIDS_TO_BE_NEARBY_RELATIVE_TO_AVERAGE_BID);
    }

    /**
     * Adds a value to the opponent analysis by forwarding it to the sub tools.
     *
     * @param value the value to be added
     */
    @Override
    public void addValue(int value) {
        patternRecognitionTool.addValue(value);
        linearRegressionTool.addValue(value);
        averagePredictionTool.addValue(value);
    }

    /**
     * Gives an estimation about the next bid by combining the results of the sub tools.
     *
     * @return an estimation about the next bid
     */
    public Estimation getEstimation() {
        Estimation patternRecognitionEstimation = patternRecognitionTool.getEstimation();
        Estimation linearRegressionEstimation = linearRegressionTool.getEstimation();
        Estimation averageValueEstimation = averagePredictionTool.getEstimation();

        Estimation combinedEstimation = EstimationCombination.combineEstimations(linearRegressionEstimation,
                averageValueEstimation, acceptedDifferenceForBidsToBeNearby);
        return EstimationCombination.combineEstimations(combinedEstimation,
                patternRecognitionEstimation, acceptedDifferenceForBidsToBeNearby);
    }

}
