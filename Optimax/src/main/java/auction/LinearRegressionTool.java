package auction;

import org.apache.commons.math3.stat.regression.SimpleRegression;

import java.util.LinkedList;
import java.util.Queue;

/**
 * An analysis tool that applies a linear regression to recent values to give an estimation about the next value. The
 * coefficient of determination or R square is used to assess the quality of the estimation. The regression
 * calculations are based on the the {@link org.apache.commons.math3.stat.regression.SimpleRegression} library.
 */
public class LinearRegressionTool implements AnalysisTool {

    /**
     * Defines the thresholds for R square to assess the quality of the estimation.
     */
    private static final double R_SQUARE_REQUIRED_FOR_ESTIMATION_QUALITY_HIGH = 0.8;
    private static final double R_SQUARE_REQUIRED_FOR_ESTIMATION_QUALITY_MIDDLE = 0.4;

    private final SimpleRegression simpleRegression;
    private final Queue<Integer> recentElements;
    private final int maxNumberOfRecentElementsToAnalyse;
    private int numberOfAllElements;

    /**
     * Initializes the tool with the maximum number of recent values that are to analyse.
     *
     * @param maxNumberOfRecentElementsToAnalyse the maximum number of recent values that are to analyse
     */
    public LinearRegressionTool(int maxNumberOfRecentElementsToAnalyse) {
        this.simpleRegression = new SimpleRegression();
        recentElements = new LinkedList<>();
        this.maxNumberOfRecentElementsToAnalyse = maxNumberOfRecentElementsToAnalyse;
        this.numberOfAllElements = 0;
    }

    /**
     * Adds a value to the sequence. If the number of maximum recent values is reached, the oldest value is discarded
     * as it is not needed anymore.
     *
     * @param value the value to be added
     */
    @Override
    public void addValue(int value) {
        numberOfAllElements++;
        simpleRegression.addData(numberOfAllElements, value);
        recentElements.add(value);

        if (numberOfAllElements > maxNumberOfRecentElementsToAnalyse) {
            double x = numberOfAllElements - maxNumberOfRecentElementsToAnalyse;
            double y = recentElements.remove();
            simpleRegression.removeData(x, y);
        }
    }

    /**
     * Gives an estimation about the next value that is based on the linear regression of recent values. The
     * coefficient of determination is used to assess the quality of the estimation. At least 2 values must have been
     * added before to deliver a significant estimation.
     *
     * @return an estimation about the next value
     */
    @Override
    public Estimation getEstimation() {
        if (numberOfAllElements < 2) {
            return new Estimation(0, EstimationQuality.ZERO);
        } else {
            int prediction = (int) Math.round(simpleRegression.predict(numberOfAllElements + 1));
            int estimation = Math.max(prediction, 0);

            double rSquare = simpleRegression.getRSquare();

            EstimationQuality estimationQuality;
            if (rSquare >= R_SQUARE_REQUIRED_FOR_ESTIMATION_QUALITY_HIGH) {
                estimationQuality = EstimationQuality.HIGH;
            } else if (rSquare >= R_SQUARE_REQUIRED_FOR_ESTIMATION_QUALITY_MIDDLE) {
                estimationQuality = EstimationQuality.MIDDLE;
            } else if (rSquare > 0.0) {
                estimationQuality = EstimationQuality.LOW;
            } else {
                estimationQuality = EstimationQuality.ZERO;
            }

            return new Estimation(estimation, estimationQuality);
        }
    }

}
