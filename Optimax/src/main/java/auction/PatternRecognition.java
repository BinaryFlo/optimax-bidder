package auction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A tool that takes a sequence of elements of the same type T to provide possible successors and their probabilities.
 * A pattern is defined as a part of the sequence with arbitrary length and it may occur several times in the sequence.
 * For each occurrence of pattern the element that comes after it (a successor) is of interest. To avoid recurring
 * calculations, all existing patterns are mapped to their successors and the frequency of each successor.
 *
 * @param <T> the type of elements to be analyzed
 */
public class PatternRecognition<T> {

    private final List<T> sequence;
    private final Map<List<T>, Map<T, Integer>> patternSuccessors;

    /**
     * Initializes the tool
     */
    public PatternRecognition() {
        sequence = new ArrayList<>();
        patternSuccessors = new HashMap<>();
    }

    /**
     * Adds an element to the sequence. All suffixes of the sequence in its old state become new patterns with the
     * newly added element as successor.
     *
     * @param element the element to be added
     */
    public void addElementToSequence(T element) {
        for (List<T> pattern : getListOfSuffixesBeforeIndex(sequence.size())) {
            if (patternSuccessors.containsKey(pattern)) {
                Map<T, Integer> successorFrequencies = patternSuccessors.get(pattern);

                if (successorFrequencies.containsKey(element)) {
                    successorFrequencies.put(element, successorFrequencies.get(element) + 1);
                } else {
                    successorFrequencies.put(element, 1);
                }
            } else {
                Map<T, Integer> successorFrequencies = new HashMap<>();
                successorFrequencies.put(element, 1);
                patternSuccessors.put(pattern, successorFrequencies);
            }
        }

        sequence.add(element);
    }

    /**
     * Returns a map of possible successors of the current end of the sequence and their probabilities based on
     * patterns that occurred in the sequence before.
     * Let the length of the sequence be l. To calculate the probability of the successors, all suffixes (patterns) of
     * the sequence with a maximum length of l/2 are evaluated. If a pattern occurred in the sequence before, each of
     * the patterns successor frequencies is multiplied by the pattern length and added to the probability value of the
     * successor. It is thereby not a classical probability but the term is used here as simplification.
     * <p>
     * Example: If the sequence is [1, 2, 3, 4, 1, 2, 3] then 4 is a successor with probability 6, as the suffixes
     * [3], [2,3], [1,2,3] have occurred exactly once in the sequence before with 4 as successor and the sum of their
     * length equals 6.
     *
     * @return possible successors and their probabilities
     */
    public Map<T, Integer> getSuccessorProbabilities() {
        Map<T, Integer> successorProbabilities = new HashMap<>();
        int maxPatternLength = sequence.size() / 2;

        for (List<T> pattern : getListOfSuffixesBeforeIndex(sequence.size(), maxPatternLength)) {
            Integer patternLength = pattern.size();
            if (patternSuccessors.containsKey(pattern)) {
                for (Map.Entry<T, Integer> successorFrequency : patternSuccessors.get(pattern).entrySet()) {
                    T successor = successorFrequency.getKey();
                    Integer weightedFrequency = successorFrequency.getValue() * patternLength;

                    if (successorProbabilities.containsKey(successor)) {
                        successorProbabilities.put(successor,
                                successorProbabilities.get(successor) + weightedFrequency);
                    } else {
                        successorProbabilities.put(successor, weightedFrequency);
                    }
                }
            }
        }

        return successorProbabilities;
    }

    /**
     * Provides a list of suffixes with a specified maximum length before a specified index of the sequence.
     *
     * @param index           the index suffixes are taken from before (exclusive)
     * @param maxSuffixLength the maximum length of the suffixes
     * @return the list of suffixes with specified maximum length before the specified index
     */
    private List<List<T>> getListOfSuffixesBeforeIndex(int index, int maxSuffixLength) {
        List<List<T>> suffixes = new ArrayList<>(maxSuffixLength);

        for (int i = index - 1; i >= Math.max(index - maxSuffixLength, 0); i--) {
            List<T> suffix = new ArrayList<>(sequence.subList(i, index));
            suffixes.add(suffix);
        }

        return suffixes;
    }

    /**
     * Provides a list of all suffixes before a specified index of the sequence.
     *
     * @param index the index suffixes are taken from before (exclusive)
     * @return the list of suffixes before the specified index
     */
    private List<List<T>> getListOfSuffixesBeforeIndex(int index) {
        return getListOfSuffixesBeforeIndex(index, index);
    }

}
