package auction;

import java.util.LinkedList;
import java.util.Queue;

/**
 * A simple analysis tool that calculates the average of recent values to give an estimation about the next value. The
 * average deviation of the values to that average is used to determine the quality of the estimation.
 */
public class AveragePredictionTool implements AnalysisTool {

    /**
     * Defines the deviation thresholds to assess the quality of the estimation. They are chosen relative to the
     * expected overall average.
     */
    private static final double
            MAX_AVERAGE_DEVIATION_FOR_ESTIMATION_QUALITY_HIGH_RELATIVE_TO_OVERALL_AVERAGE = 0.1;
    private static final double
            MAX_AVERAGE_DEVIATION_FOR_ESTIMATION_QUALITY_MIDDLE_RELATIVE_TO_OVERALL_AVERAGE = 0.25;

    private final Queue<Integer> recentValues;
    private final int maxNumberOfRecentValuesToAnalyse;
    private int numberOfRecentValues;
    private final double maxAverageDeviationForEstimationQualityHigh;
    private final double maxAverageDeviationForEstimationQualityMiddle;

    /**
     * Initializes the tool with the maximum number of recent values that are to analyse and the expected overall
     * average (which is the expected average of all values that will be part of the sequence).
     *
     * @param maxNumberOfRecentValuesToAnalyse the maximum number of recent values that are to analyse
     * @param overallAverage                   the expected overall average
     */
    public AveragePredictionTool(int maxNumberOfRecentValuesToAnalyse, int overallAverage) {
        this.maxNumberOfRecentValuesToAnalyse = maxNumberOfRecentValuesToAnalyse;
        this.recentValues = new LinkedList<>();
        numberOfRecentValues = 0;
        maxAverageDeviationForEstimationQualityHigh = overallAverage *
                MAX_AVERAGE_DEVIATION_FOR_ESTIMATION_QUALITY_HIGH_RELATIVE_TO_OVERALL_AVERAGE;
        maxAverageDeviationForEstimationQualityMiddle = overallAverage *
                MAX_AVERAGE_DEVIATION_FOR_ESTIMATION_QUALITY_MIDDLE_RELATIVE_TO_OVERALL_AVERAGE;
    }

    /**
     * Adds a value to the sequence. If the number of maximum recent values is reached, the oldest value will be
     * discarded as it is not needed anymore.
     *
     * @param value the value to be added
     */
    @Override
    public void addValue(int value) {
        recentValues.add(value);
        if (numberOfRecentValues >= maxNumberOfRecentValuesToAnalyse) {
            recentValues.remove();
        } else {
            numberOfRecentValues++;
        }
    }

    /**
     * Gives an estimation about the next value that is based on the average of recent values. The deviation of those
     * values to the average is used to assess the quality of the estimation. At least 2 values must have been added
     * before to deliver a significant estimation.
     *
     * @return an estimation about the next value
     */
    @Override
    public Estimation getEstimation() {
        if (numberOfRecentValues < 2) {
            return new Estimation(0, EstimationQuality.ZERO);
        } else {
            int sumOfValues = 0;
            for (Integer value : recentValues) {
                sumOfValues += value;
            }
            double averageValue = (double) sumOfValues / numberOfRecentValues;

            double sumOfDeviations = 0;
            for (Integer value : recentValues) {
                double deviation = Math.abs(value - averageValue);
                sumOfDeviations += deviation;
            }
            double averageDeviation = sumOfDeviations / numberOfRecentValues;

            int estimatedValue = (int) Math.round(averageValue);
            EstimationQuality estimationQuality;

            if (averageDeviation <= maxAverageDeviationForEstimationQualityHigh) {
                estimationQuality = EstimationQuality.HIGH;
            } else if (averageDeviation <= maxAverageDeviationForEstimationQualityMiddle) {
                estimationQuality = EstimationQuality.MIDDLE;
            } else {
                estimationQuality = EstimationQuality.LOW;
            }

            return new Estimation(estimatedValue, estimationQuality);
        }
    }
}
