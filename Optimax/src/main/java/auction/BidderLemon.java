package auction;

/**
 * A super smart bidder wih a cool name that will win every auction.
 */
public class BidderLemon implements Bidder {

    /**
     * Defines the number of quantity units per round of the auction, usually 2 but may be any multiple of 2.
     */
    private static final int NUMBER_OF_QUANTITY_UNITS_PER_ROUND = 2;

    /**
     * Defines the length of the initial phase.
     */
    private static final double RELATIVE_LENGTH_OF_INITIAL_PHASE = 0.1;

    /**
     * Defines the threshold to be close to winning.
     */
    private static final double RELATIVE_QUANTITY_NEEDED_TO_BE_CLOSE_TO_WINNING = 0.85;

    /**
     * Defines the area around the average bid that counts as average.
     */
    private static final double RELATIVE_DEVIATION_OF_AVERAGE_BID_AREA = 0.2;

    /**
     * Defines a small bid, it is larger than 0 in case the opponents bid is 0 as well.
     */
    private static final int SMALL_BID = 1;

    /**
     * Defines the multiplier that determines the own bid if going slightly higher that the opponent.
     */
    private static final double MULTIPLIER_IF_GOING_SLIGHTLY_HIGHER = 1.2;

    /**
     * Defines the multiplier that determines the own bid if going significantly higher that the opponent.
     */
    private static final double MULTIPLIER_IF_GOING_SIGNIFICANTLY_HIGHER = 1.5;

    private int accumulatedQuantity;
    private int accumulatedQuantityOther;
    int quantityNeededToWin;
    private int remainingCash;
    private int numberOfRounds;
    private int completedRounds;
    private int averageBid;
    private boolean opponentIsCloseToWinning;
    private int deviationOfAverageArea;
    private OpponentAnalysis opponentAnalysis;

    /**
     * Creates the bidder. Calling {@link this#init(int, int)} is required to initialize completely.
     */
    public BidderLemon() {
    }

    /**
     * Initializes the bidder with the production quantity and the allowed cash limit.
     *
     * @param quantity the quantity
     * @param cash     the cash limit
     */
    @Override
    public void init(int quantity, int cash) {
        accumulatedQuantity = 0;
        accumulatedQuantityOther = 0;
        quantityNeededToWin = quantity / NUMBER_OF_QUANTITY_UNITS_PER_ROUND;
        remainingCash = cash;
        numberOfRounds = quantity / NUMBER_OF_QUANTITY_UNITS_PER_ROUND;
        completedRounds = 0;
        averageBid = cash / numberOfRounds;
        opponentIsCloseToWinning = false;
        deviationOfAverageArea = (int) Math.round(averageBid * RELATIVE_DEVIATION_OF_AVERAGE_BID_AREA);
        opponentAnalysis = new OpponentAnalysis(averageBid);
    }

    /**
     * Retrieves the next bid for the product, which may be zero. Uses opponent analysis to get an estimation for the
     * next bid of the opponent and decides about its own bid upon the estimation and other factors.
     *
     * @return the next bid
     */
    @Override
    public int placeBid() {
        // Check if we already won
        if (accumulatedQuantity > quantityNeededToWin) {
            return 0;
        }

        int nextOwnBid = 0;

        // Check if we're still in the initial phase
        if (completedRounds < numberOfRounds * RELATIVE_LENGTH_OF_INITIAL_PHASE) {
            // To get a grasp of the opponent we'll bid randomly in the average area
            double deviation = (((Math.random() * 2) - 1) * deviationOfAverageArea);
            nextOwnBid = (int) Math.round(averageBid + deviation);

        } else {
            // Get an estimation of the next bid from the opponent analysis
            Estimation estimation = opponentAnalysis.getEstimation();
            int estimatedNextBid = estimation.getEstimatedValue();
            EstimationQuality estimationQuality = estimation.getEstimationQuality();

            // Check if the opponent is close to winning, if not then we are in the main phase
            if (!opponentIsCloseToWinning) {
                // Determine whether the estimated bid is below, above or about average
                BidArea estimatedBidArea;
                if (estimatedNextBid < averageBid - deviationOfAverageArea) {
                    estimatedBidArea = BidArea.BELOW_AVERAGE;
                } else if (estimatedNextBid > averageBid - deviationOfAverageArea) {
                    estimatedBidArea = BidArea.ABOVE_AVERAGE;
                } else {
                    estimatedBidArea = BidArea.AVERAGE;
                }

                if (estimationQuality == EstimationQuality.HIGH) {
                    // If the estimation quality is good, we can safely go a bit higher
                    nextOwnBid = slightlyHigherBid(estimatedNextBid);

                } else if (estimationQuality == EstimationQuality.MIDDLE) {
                    // If the quality is middle and the estimation is above average, we are playing safe and going low
                    if (estimatedBidArea == BidArea.ABOVE_AVERAGE) {
                        nextOwnBid = SMALL_BID;
                    }

                    // If the quality is middle and the estimation is average or below, we are going significantly
                    // higher
                    else {
                        nextOwnBid = significantlyHigherBid(estimatedNextBid);
                    }

                } else if (estimationQuality == EstimationQuality.LOW) {
                    // If the quality is low and the estimation is above average, we are playing safe and going low
                    if (estimatedBidArea == BidArea.ABOVE_AVERAGE) {
                        nextOwnBid = SMALL_BID;
                    }

                    // If the quality is low and the estimation is average, we are tossing a coin to either go low or
                    // significantly higher than average
                    else if (estimatedBidArea == BidArea.AVERAGE) {
                        double tossACoin = Math.random();
                        if (tossACoin < 0.5) {
                            nextOwnBid = SMALL_BID;
                        } else {
                            nextOwnBid = significantlyHigherBid(averageBid);
                        }
                    }

                    // If the quality is low and the estimation is below average, we take the average bid
                    else {
                        nextOwnBid = averageBid;
                    }
                }

                // If the estimation quality is zero, we are playing safe and going low
                else {
                    nextOwnBid = SMALL_BID;
                }

            }

            // If the opponent is close to winning, we can't afford losing any more rounds so we don't care anymore
            // whether the estimation is above or below average
            else {
                // If the estimation quality is high, it's enough if we go slightly higher
                if (estimationQuality == EstimationQuality.HIGH) {
                    nextOwnBid = slightlyHigherBid(estimatedNextBid);
                }

                // If the quality is zero, we are assuming an average bid
                else if (estimationQuality == EstimationQuality.ZERO) {
                    nextOwnBid = significantlyHigherBid(averageBid);

                    // If the quality is middle or low, we are going significantly higher
                } else {
                    nextOwnBid = significantlyHigherBid(estimatedNextBid);
                }
            }
        }

        // Ensure that we don't bid more than our remaining cash
        return Math.min(nextOwnBid, remainingCash);
    }

    /**
     * Returns a bid that is slightly higher than the given estimated bid to top it.
     *
     * @param estimatedBid the estimated bid that needs to be topped.
     * @return a bid slightly larger than the estimated bid
     */
    private int slightlyHigherBid(int estimatedBid) {
        return Math.max(estimatedBid + 1, (int) Math.round(estimatedBid * MULTIPLIER_IF_GOING_SLIGHTLY_HIGHER));
    }

    /**
     * Returns a bid that is significantly higher than the given estimated bid to top it.
     *
     * @param estimatedBid the estimated bid that needs to be topped.
     * @return a bid significantly larger than the estimated bid
     */
    private int significantlyHigherBid(int estimatedBid) {
        return Math.max(estimatedBid + 1, (int) Math.round(estimatedBid * MULTIPLIER_IF_GOING_SIGNIFICANTLY_HIGHER));
    }

    /**
     * Shows the bids of the two bidders.
     *
     * @param own   the bid of this bidder
     * @param other the bid of the other bidder
     */
    @Override
    public void bids(int own, int other) {
        // Check who has won the last round and increment the accumulated quantities accordingly
        if (own > other) {
            accumulatedQuantity += NUMBER_OF_QUANTITY_UNITS_PER_ROUND;
        } else if (other > own) {
            accumulatedQuantityOther += NUMBER_OF_QUANTITY_UNITS_PER_ROUND;
        } else {
            accumulatedQuantity += NUMBER_OF_QUANTITY_UNITS_PER_ROUND / 2;
            accumulatedQuantityOther += NUMBER_OF_QUANTITY_UNITS_PER_ROUND / 2;
        }

        // Update the other parameters
        remainingCash -= own;
        opponentAnalysis.addValue(other);
        completedRounds += 1;

        // Check if the opponent is close to winning
        if (accumulatedQuantityOther >= RELATIVE_QUANTITY_NEEDED_TO_BE_CLOSE_TO_WINNING * quantityNeededToWin) {
            opponentIsCloseToWinning = true;
        }
    }

}
