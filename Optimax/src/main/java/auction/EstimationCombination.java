package auction;

/**
 * A tool to combine estimations.
 */
public abstract class EstimationCombination {

    /**
     * Combines two estimations based on their values and qualities and returns a new. Their order is irrelevant. The
     * estimations are combined differently whether their values are nearby or not. For instance, the case where two
     * estimations are both of high quality but have significantly different values needs to be treated differently
     * to the case where those estimations have values that are nearby. Thereby a maximum difference for the values to
     * be treated as nearby is required.
     *
     * @param estimationA                      one estimation to be combined
     * @param estimationB                      the other estimation to be combined
     * @param maxDifferenceForValuesToBeNearby maximum difference for values to be treated as nearby
     * @return the combined estimation
     */
    public static Estimation combineEstimations(Estimation estimationA, Estimation estimationB,
                                                int maxDifferenceForValuesToBeNearby) {
        int estimatedValueA = estimationA.getEstimatedValue();
        int estimatedValueB = estimationB.getEstimatedValue();
        EstimationQuality estimationQualityA = estimationA.getEstimationQuality();
        EstimationQuality estimationQualityB = estimationB.getEstimationQuality();

        if (estimationQualityA == EstimationQuality.ZERO) {
            return new Estimation(estimatedValueB, estimationQualityB);
        } else if (estimationQualityB == EstimationQuality.ZERO) {
            return new Estimation(estimatedValueA, estimationQualityA);
        }

        boolean valuesAreNearby = false;
        if (Math.abs(estimatedValueA - estimatedValueB) <= maxDifferenceForValuesToBeNearby) {
            valuesAreNearby = true;
        }

        if (valuesAreNearby) {
            int largerEstimatedBid = Math.max(estimatedValueA, estimatedValueB);
            switch (estimationQualityA) {
                case HIGH:
                    switch (estimationQualityB) {
                        case HIGH:
                            return new Estimation(largerEstimatedBid, EstimationQuality.HIGH);
                        case MIDDLE:
                        case LOW:
                            return new Estimation(estimatedValueA, EstimationQuality.HIGH);
                    }
                    break;
                case MIDDLE:
                    switch (estimationQualityB) {
                        case HIGH:
                            return new Estimation(estimatedValueB, EstimationQuality.HIGH);
                        case MIDDLE:
                            return new Estimation(largerEstimatedBid, EstimationQuality.HIGH);
                        case LOW:
                            return new Estimation(estimatedValueA, EstimationQuality.MIDDLE);
                    }
                    break;
                case LOW:
                    switch (estimationQualityB) {
                        case HIGH:
                            return new Estimation(estimatedValueB, EstimationQuality.HIGH);
                        case MIDDLE:
                            return new Estimation(estimatedValueB, EstimationQuality.MIDDLE);
                        case LOW:
                            return new Estimation(largerEstimatedBid, EstimationQuality.LOW);
                    }
                    break;
            }
        } else {
            switch (estimationQualityA) {
                case HIGH:
                    switch (estimationQualityB) {
                        case HIGH:
                            return new Estimation((estimatedValueA + estimatedValueB) / 2,
                                    EstimationQuality.MIDDLE);
                        case MIDDLE:
                        case LOW:
                            return new Estimation(estimatedValueA, EstimationQuality.HIGH);
                    }
                case MIDDLE:
                    switch (estimationQualityB) {
                        case HIGH:
                            return new Estimation(estimatedValueB, EstimationQuality.HIGH);
                        case MIDDLE:
                            return new Estimation((estimatedValueA + estimatedValueB) / 2,
                                    EstimationQuality.MIDDLE);
                        case LOW:
                            return new Estimation(estimatedValueA, EstimationQuality.MIDDLE);
                    }
                case LOW:
                    switch (estimationQualityB) {
                        case HIGH:
                            return new Estimation(estimatedValueB, EstimationQuality.HIGH);
                        case MIDDLE:
                            return new Estimation(estimatedValueB, EstimationQuality.MIDDLE);
                        case LOW:
                            return new Estimation((estimatedValueA + estimatedValueB) / 2,
                                    EstimationQuality.LOW);
                    }
            }
        }

        return new Estimation(0, EstimationQuality.ZERO);
    }

}
