package auction;

import java.util.Map;

/**
 * An analysis tool that uses discrete pattern recognition on a sequence of values to give an estimation about the
 * next value. The number of recognized patterns weighted by their length is used to assess the quality of the
 * estimation.
 */
public class PatternRecognitionTool implements AnalysisTool {

    /**
     * Defines the successor probability thresholds to assess the quality of the estimation. The calculation of the
     * probability values is described here: {@link PatternRecognition#getSuccessorProbabilities()}.
     */
    public static final int SUCCESSOR_PROBABILITY_REQUIRED_FOR_ESTIMATION_QUALITY_HIGH = 10;
    public static final int SUCCESSOR_PROBABILITY_REQUIRED_FOR_ESTIMATION_QUALITY_MIDDLE = 5;

    private final PatternRecognition<Integer> patternRecognition;

    /**
     * Initializes the tool.
     */
    public PatternRecognitionTool() {
        patternRecognition = new PatternRecognition<>();
    }

    /**
     * Adds a value to the sequence.
     *
     * @param value the value to be added
     */
    @Override
    public void addValue(int value) {
        patternRecognition.addElementToSequence(value);
    }

    /**
     * Gives an estimation about the next value that is based on pattern recognition. The number of recognized
     * patterns weighted by their length is used to assess the quality of the estimation.
     *
     * @return an estimation about the next value
     */
    @Override
    public Estimation getEstimation() {
        Map<Integer, Integer> successorProbabilities = patternRecognition.getSuccessorProbabilities();

        if (!successorProbabilities.keySet().isEmpty()) {
            int mostLikelySuccessor = 0;
            int highestSuccessorProbability = 0;
            for (Map.Entry<Integer, Integer> successorProbability : successorProbabilities.entrySet()) {
                if (successorProbability.getValue() > highestSuccessorProbability) {
                    mostLikelySuccessor = successorProbability.getKey();
                    highestSuccessorProbability = successorProbability.getValue();
                }
            }

            EstimationQuality estimationQuality;
            if (highestSuccessorProbability >= SUCCESSOR_PROBABILITY_REQUIRED_FOR_ESTIMATION_QUALITY_HIGH) {
                estimationQuality = EstimationQuality.HIGH;
            } else if (highestSuccessorProbability >= SUCCESSOR_PROBABILITY_REQUIRED_FOR_ESTIMATION_QUALITY_MIDDLE) {
                estimationQuality = EstimationQuality.MIDDLE;
            } else {
                estimationQuality = EstimationQuality.LOW;
            }
            return new Estimation(mostLikelySuccessor, estimationQuality);

        } else {
            return new Estimation(0, EstimationQuality.ZERO);
        }
    }
}
