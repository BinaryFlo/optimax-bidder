package auction;

/**
 * Levels of bid areas
 */
public enum BidArea {
    AVERAGE,
    BELOW_AVERAGE,
    ABOVE_AVERAGE
}
