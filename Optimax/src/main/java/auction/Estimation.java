package auction;

import java.util.Objects;

/**
 * An estimation consisting of the estimated value and the quality of the estimation.
 */
public class Estimation {

    private final int estimatedValue;
    private final EstimationQuality estimationQuality;

    /**
     * Initializes an estimation with the estimated value and the quality of the estimation.
     *
     * @param estimatedValue    the estimated value
     * @param estimationQuality teh quality of the estimation
     */
    public Estimation(int estimatedValue, EstimationQuality estimationQuality) {
        this.estimatedValue = estimatedValue;
        this.estimationQuality = estimationQuality;
    }

    /**
     * @return the estimated value
     */
    public Integer getEstimatedValue() {
        return estimatedValue;
    }

    /**
     * @return the quality of the estimation
     */
    public EstimationQuality getEstimationQuality() {
        return estimationQuality;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Estimation that = (Estimation) o;
        return estimatedValue == that.estimatedValue && estimationQuality == that.estimationQuality;
    }

    @Override
    public int hashCode() {
        return Objects.hash(estimatedValue, estimationQuality);
    }
}
