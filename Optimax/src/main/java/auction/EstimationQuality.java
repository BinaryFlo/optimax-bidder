package auction;

/**
 * Levels of quality of an estimation.
 */
public enum EstimationQuality {
    ZERO,
    LOW,
    MIDDLE,
    HIGH
}
