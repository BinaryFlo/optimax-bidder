package auction;

/**
 * Represents an auction
 */
public class Auction {

    /**
     * Starts an auction between two bidders with given quantity and cash. Will output the process of the auction to
     * the console if not silenced to help analyze the bidder behavior.
     *
     * @param bidderA  one bidder
     * @param bidderB  the other bidder
     * @param quantity the quantity to be auctioned
     * @param cash     the cash each bidder has
     * @param silent   suppresses console output
     * @return the winner of the auction
     * @throws IllegalArgumentException if quantity or cash have illegal values
     */
    public static Bidder startAuction(Bidder bidderA, Bidder bidderB, int quantity, int cash, boolean silent)
            throws IllegalArgumentException {
        if (quantity % 2 != 0) {
            throw new IllegalArgumentException("Quantity must be a multiple of 2");
        } else if (quantity < 2) {
            throw new IllegalArgumentException("Quantity must be greater or equal to 2");
        } else if (cash < 1) {
            throw new IllegalArgumentException("Cash must be greater or equal to 1");
        }

        int remainingQuantity = quantity;
        int remainingCashBidderA = cash;
        int remainingCashBidderB = cash;
        int accumulatedQuantityBidderA = 0;
        int accumulatedQuantityBidderB = 0;
        int roundNumber = 0;

        bidderA.init(quantity, cash);
        bidderB.init(quantity, cash);

        StringBuilder output = new StringBuilder();

        output.append("The auction starts with quantity ").append(quantity).append(" and cash ").append(cash).append(
                "\n");
        output.append("Round\t\tBid A\t\tBid B\t\tWinner\t\tQuantity A\tCash A\t\tQuantity B\tCash B\n");

        while (remainingQuantity > 0) {
            int bidA = bidderA.placeBid();
            int bidB = bidderB.placeBid();

            if (bidA < 0) {
                output.append("Bid ").append(bidA).append(" from BidderA is negative. It will be treated as 0.");
                bidA = 0;
            }

            if (bidB < 0) {
                output.append("Bid ").append(bidB).append(" from BidderB is negative. It will be treated as 0.");
                bidB = 0;
            }

            if (bidA > remainingCashBidderA) {
                output.append("Bid ").append(bidA).append(" from BidderA is more than its remaining cash. ").append(
                        "It will be treated as the remaining ").append(remainingCashBidderA).append(".\n");
                bidA = remainingCashBidderA;
            }

            if (bidB > remainingCashBidderB) {
                output.append("Bid ").append(bidB).append(" from BidderB is more than its remaining cash. ").append(
                        "It will be treated as the remaining ").append(remainingCashBidderB).append(".\n");
                bidB = remainingCashBidderB;
            }

            String winner;
            if (bidA > bidB) {
                accumulatedQuantityBidderA += 2;
                winner = "A";
            } else if (bidB > bidA) {
                accumulatedQuantityBidderB += 2;
                winner = "B";
            } else {
                accumulatedQuantityBidderA += 1;
                accumulatedQuantityBidderB += 1;
                winner = "A,B";
            }

            remainingQuantity -= 2;
            remainingCashBidderA -= bidA;
            remainingCashBidderB -= bidB;
            roundNumber++;

            output.append(roundNumber).append("\t\t\t").append(bidA).append("\t\t\t").append(bidB).append("\t\t\t")
                    .append(winner).append("\t\t\t").append(accumulatedQuantityBidderA).append("\t\t\t").append(
                    remainingCashBidderA).append("\t\t\t").append(accumulatedQuantityBidderB).append("\t\t\t").append(
                    remainingCashBidderB).append("\n");

            bidderA.bids(bidA, bidB);
            bidderB.bids(bidB, bidA);
        }

        output.append("The auction is over.\n");
        output.append("BidderA has accumulated ").append(accumulatedQuantityBidderA).append(" quantity with ").append(
                remainingCashBidderA).append(" cash left.\n");
        output.append("BidderB has accumulated ").append(accumulatedQuantityBidderB).append(" quantity with ").append(
                remainingCashBidderB).append(" cash left.\n");

        if (!silent) {
            System.out.println(output);
        }

        if (accumulatedQuantityBidderA > accumulatedQuantityBidderB) {
            return bidderA;
        } else if (accumulatedQuantityBidderB > accumulatedQuantityBidderA) {
            return bidderB;
        } else if (remainingCashBidderA > remainingCashBidderB) {
            return bidderA;
        } else if (remainingCashBidderB > remainingCashBidderA) {
            return bidderB;
        } else {
            return null;
        }
    }

}
