package auction;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class AveragePredictionToolTest {

    @Test
    public void testAveragePredictionTool() {
        AveragePredictionTool averagePredictionTool1 = new AveragePredictionTool(8, 20);
        List<Integer> sequence1a = Collections.emptyList();
        Estimation expectedEstimation1a = new Estimation(0, EstimationQuality.ZERO);

        AnalysisToolTest.addSequenceToAnalysisToolAndAssertEstimation(averagePredictionTool1, sequence1a,
                expectedEstimation1a);

        List<Integer> sequence1b = Arrays.asList(1, 2, 3);
        Estimation expectedEstimation1b = new Estimation(2, EstimationQuality.HIGH);

        AnalysisToolTest.addSequenceToAnalysisToolAndAssertEstimation(averagePredictionTool1, sequence1b,
                expectedEstimation1b);

        List<Integer> sequence1c = Arrays.asList(6, 3, 8, 5, 9, 11, 4);
        Estimation expectedEstimation1c = new Estimation(6, EstimationQuality.MIDDLE);

        AnalysisToolTest.addSequenceToAnalysisToolAndAssertEstimation(averagePredictionTool1, sequence1c,
                expectedEstimation1c);

        AveragePredictionTool averagePredictionTool2 = new AveragePredictionTool(5, 10);
        List<Integer> sequence2a = Arrays.asList(5, 3, 9, 10);
        Estimation expectedEstimation2a = new Estimation(7, EstimationQuality.LOW);

        AnalysisToolTest.addSequenceToAnalysisToolAndAssertEstimation(averagePredictionTool2, sequence2a,
                expectedEstimation2a);

        List<Integer> sequence2b = Arrays.asList(12, 11, 8);
        Estimation expectedEstimation2b = new Estimation(10, EstimationQuality.MIDDLE);

        AnalysisToolTest.addSequenceToAnalysisToolAndAssertEstimation(averagePredictionTool2, sequence2b,
                expectedEstimation2b);
    }

}
