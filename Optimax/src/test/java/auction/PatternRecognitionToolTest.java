package auction;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class PatternRecognitionToolTest {

    @Test
    public void testPatternRecognitionTool() {
        PatternRecognitionTool patternRecognitionTool = new PatternRecognitionTool();
        List<Integer> sequence1 = Arrays.asList(1, 2, 3, 4);
        Estimation expectedEstimation1 = new Estimation(0, EstimationQuality.ZERO);

        AnalysisToolTest.addSequenceToAnalysisToolAndAssertEstimation(patternRecognitionTool, sequence1,
                expectedEstimation1);

        List<Integer> sequence2 = Arrays.asList(1, 2, 3);
        Estimation expectedEstimation2 = new Estimation(4, EstimationQuality.MIDDLE);

        AnalysisToolTest.addSequenceToAnalysisToolAndAssertEstimation(patternRecognitionTool, sequence2,
                expectedEstimation2);

        List<Integer> sequence3 = Collections.singletonList(1);
        Estimation expectedEstimation3 = new Estimation(2, EstimationQuality.LOW);

        AnalysisToolTest.addSequenceToAnalysisToolAndAssertEstimation(patternRecognitionTool, sequence3,
                expectedEstimation3);

        List<Integer> sequence4 = Arrays.asList(2, 3, 4);
        Estimation expectedEstimation4 = new Estimation(1, EstimationQuality.HIGH);

        AnalysisToolTest.addSequenceToAnalysisToolAndAssertEstimation(patternRecognitionTool, sequence4,
                expectedEstimation4);
    }
}
