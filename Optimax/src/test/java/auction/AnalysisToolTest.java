package auction;

import org.junit.jupiter.api.Assertions;

import java.util.List;

public abstract class AnalysisToolTest {

    public static void addSequenceToAnalysisToolAndAssertEstimation(
            AnalysisTool analysisTool, List<Integer> sequence, Estimation expectedEstimation) {
        for (Integer value : sequence) {
            analysisTool.addValue(value);
        }

        Estimation estimation = analysisTool.getEstimation();
        Assertions.assertEquals(expectedEstimation, estimation);
    }

}
