package auction;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PatternRecognitionTest {

    @Test
    public void testPatternRecognition() {
        PatternRecognition<Integer> patternRecognition1 = new PatternRecognition<>();
        List<Integer> sequence1 = Arrays.asList(1, 2, 3, 4, 1, 2, 3);
        Map<Integer, Integer> expectedSuccessorProbabilities1 = new HashMap<>();
        expectedSuccessorProbabilities1.put(4, 6);

        addSequenceToPatternRecognitionAndAssertSuccessorProbabilities(
                patternRecognition1, sequence1, expectedSuccessorProbabilities1);

        PatternRecognition<String> patternRecognition2 = new PatternRecognition<>();
        List<String> sequence2 = Arrays.asList("A", "X", "A", "X", "A", "Y", "A", "Y", "Z", "A");
        Map<String, Integer> expectedSuccessorProbabilities2 = new HashMap<>();
        expectedSuccessorProbabilities2.put("X", 2);
        expectedSuccessorProbabilities2.put("Y", 2);

        addSequenceToPatternRecognitionAndAssertSuccessorProbabilities(
                patternRecognition2, sequence2, expectedSuccessorProbabilities2);

        PatternRecognition<Boolean> patternRecognition3 = new PatternRecognition<>();
        List<Boolean> sequence3 = Arrays.asList(true, true, true, true);
        Map<Boolean, Integer> expectedSuccessorProbabilities3 = new HashMap<>();
        expectedSuccessorProbabilities3.put(true, 7);

        addSequenceToPatternRecognitionAndAssertSuccessorProbabilities(
                patternRecognition3, sequence3, expectedSuccessorProbabilities3);
    }


    private <T> void addSequenceToPatternRecognitionAndAssertSuccessorProbabilities(
            PatternRecognition<T> patternRecognition, List<T> sequence,
            Map<T, Integer> expectedSuccessorProbabilities) {
        for (T sequenceElement : sequence) {
            patternRecognition.addElementToSequence(sequenceElement);
        }
        Map<T, Integer> successorProbabilities = patternRecognition.getSuccessorProbabilities();
        Assertions.assertEquals(expectedSuccessorProbabilities, successorProbabilities);
    }

}
