package auction;

/**
 * A simple bidder that bids randomly around the average bid.
 */
public class RandomBidder implements Bidder {

    private int remainingCash;
    private int averageBid;

    /**
     * Initializes the bidder with the production quantity and the allowed cash limit.
     *
     * @param quantity the quantity
     * @param cash     the cash limit
     */
    @Override
    public void init(int quantity, int cash) {
        remainingCash = cash;
        averageBid = cash / (quantity / 2);
    }

    /**
     * Retrieves the next bid for the product, which may be zero.
     *
     * @return the next bid
     */
    @Override
    public int placeBid() {
        return Math.min((int) ((Math.random() + 0.5) * averageBid), remainingCash);
    }

    /**
     * Shows the bids of the two bidders.
     *
     * @param own   the bid of this bidder
     * @param other the bid of the other bidder
     */
    @Override
    public void bids(int own, int other) {
        remainingCash -= own;
    }

    public RandomBidder() {
    }

}
