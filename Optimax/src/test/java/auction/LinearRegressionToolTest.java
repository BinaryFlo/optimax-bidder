package auction;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class LinearRegressionToolTest {

    @Test
    public void testLinearRegressionTool() {
        LinearRegressionTool linearRegressionTool1 = new LinearRegressionTool(8);
        List<Integer> sequence1a = Collections.emptyList();
        Estimation expectedEstimation1a = new Estimation(0, EstimationQuality.ZERO);

        AnalysisToolTest.addSequenceToAnalysisToolAndAssertEstimation(linearRegressionTool1, sequence1a,
                expectedEstimation1a);

        List<Integer> sequence1b = Arrays.asList(8, 2, 4, 5, 3);
        Estimation expectedEstimation1b = new Estimation(2, EstimationQuality.LOW);

        AnalysisToolTest.addSequenceToAnalysisToolAndAssertEstimation(linearRegressionTool1, sequence1b,
                expectedEstimation1b);

        List<Integer> sequence1c = Arrays.asList(6, 4, 6, 7);
        Estimation expectedEstimation1c = new Estimation(7, EstimationQuality.MIDDLE);

        AnalysisToolTest.addSequenceToAnalysisToolAndAssertEstimation(linearRegressionTool1, sequence1c,
                expectedEstimation1c);

        LinearRegressionTool linearRegressionTool2 = new LinearRegressionTool(10);
        List<Integer> sequence2 = Arrays.asList(3, 5, 8, 7, 10, 12, 10, 13);
        Estimation expectedEstimation2 = new Estimation(14, EstimationQuality.HIGH);

        AnalysisToolTest.addSequenceToAnalysisToolAndAssertEstimation(linearRegressionTool2, sequence2,
                expectedEstimation2);

        LinearRegressionTool linearRegressionTool3 = new LinearRegressionTool(10);
        List<Integer> sequence3 = Arrays.asList(1, 2, 1, 2, 1);
        Estimation expectedEstimation3 = new Estimation(1, EstimationQuality.ZERO);

        AnalysisToolTest.addSequenceToAnalysisToolAndAssertEstimation(linearRegressionTool3, sequence3,
                expectedEstimation3);
    }

}
