package auction;

/**
 * A simple bidder that spends all his cash on the first half of the auction.
 */
public class AllInBidder implements Bidder {

    private int bidEachRound;
    private int remainingCash;

    /**
     * Initializes the bidder with the production quantity and the allowed cash limit.
     *
     * @param quantity the quantity
     * @param cash     the cash limit
     */
    @Override
    public void init(int quantity, int cash) {
        remainingCash = cash;
        bidEachRound = cash / (int) Math.ceil(((quantity / 2.0) + 1) / 2.0);
    }

    /**
     * Retrieves the next bid for the product, which may be zero.
     *
     * @return the next bid
     */
    @Override
    public int placeBid() {
        return Math.min(bidEachRound, remainingCash);
    }

    /**
     * Shows the bids of the two bidders.
     *
     * @param own   the bid of this bidder
     * @param other the bid of the other bidder
     */
    @Override
    public void bids(int own, int other) {
        remainingCash -= own;
    }

    public AllInBidder() {
    }

}
