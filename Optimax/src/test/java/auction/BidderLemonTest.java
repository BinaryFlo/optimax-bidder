package auction;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class BidderLemonTest {

    @Test
    public void testBidderLemon() {
        auctionAgainstOpponent(new BidderLemon(), new ConstantBidder());
        auctionAgainstOpponent(new BidderLemon(), new AllInBidder());
        auctionAgainstOpponent(new BidderLemon(), new RandomBidder());
    }

    private void auctionAgainstOpponent(BidderLemon bidderLemon, Bidder other) {
        int numberOfTries = 100;
        int numberOfWinsBidderLemon = 0;
        int numberOfWinsOther = 0;

        for (int i = 0; i < numberOfTries; i++) {
            Bidder winner = Auction.startAuction(bidderLemon, other, 200, 1000, true);

            if (bidderLemon.equals(winner)) {
                numberOfWinsBidderLemon++;
            } else if (other.equals(winner)) {
                numberOfWinsOther++;
            }
        }

        System.out.println("\nBidderLemon has won " + numberOfWinsBidderLemon + " times, " + other.getClass().getName()
                + " " + numberOfWinsOther + " times");
        Assertions.assertTrue(numberOfWinsBidderLemon > numberOfWinsOther);
    }
}
