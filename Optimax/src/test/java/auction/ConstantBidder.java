package auction;

/**
 * A simple bidder that bids the same amount of cash each round of the auction.
 */
public class ConstantBidder implements Bidder {

    private int remainingQuantity;
    private int remainingCash;

    /**
     * Initializes the bidder with the production quantity and the allowed cash limit.
     *
     * @param quantity the quantity
     * @param cash     the cash limit
     */
    @Override
    public void init(int quantity, int cash) {
        remainingQuantity = quantity;
        remainingCash = cash;
    }

    /**
     * Retrieves the next bid for the product, which may be zero.
     *
     * @return the next bid
     */
    @Override
    public int placeBid() {
        return remainingCash / remainingQuantity * 2;
    }

    /**
     * Shows the bids of the two bidders.
     *
     * @param own   the bid of this bidder
     * @param other the bid of the other bidder
     */
    @Override
    public void bids(int own, int other) {
        remainingCash -= own;
        remainingQuantity -= 2;
    }

    public ConstantBidder() {
    }

}
